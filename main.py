from collections import defaultdict
import timeit
import copy

import classes
import numpy as np
from LoadFromFile import loadfromfile

basevalues = loadfromfile()

number_of_nodes = basevalues['nH'] * basevalues['nL']
list_of_nodes = []
dict_of_nodes = {}
for i in range(1, number_of_nodes + 1):  # create nodes
    if i % basevalues['nH']:
        dict_of_nodes[i] = classes.Node((i // basevalues['nH']) * basevalues['deltaL'],
                                        ((i % basevalues['nH']) - 1) * basevalues['deltaH'],
                                        basevalues['t0'], i)  # implement class
    else:
        dict_of_nodes[i] = classes.Node((i // basevalues['nH'] - 1) * (basevalues['deltaL']),
                                        basevalues['H'],
                                        basevalues['t0'], i)
    if dict_of_nodes[i].x == 0 or dict_of_nodes[i].x == basevalues['L'] or \
            dict_of_nodes[i].y == 0 or dict_of_nodes[i].y == basevalues['H']:
        dict_of_nodes[i].set_boundary_condition(True)

for i in range(1, (number_of_nodes + 1) - basevalues['nH']):  # create elements
    if i % basevalues['nH']:
        list_of_nodes.append(
            [dict_of_nodes[i], dict_of_nodes[i + basevalues['nH']], dict_of_nodes[i + basevalues['nH'] + 1],
             dict_of_nodes[i + 1]])
list_of_elements = []
for x in list_of_nodes:
    list_of_elements.append(classes.Element(x, basevalues['K']))
dict_of_elements = dict(enumerate(list_of_elements, start=1))
for k, v in dict_of_elements.items():
    v.set_id(k)
grid = classes.Grid(dict_of_nodes, dict_of_elements)
# print(grid)

print('\n\n\n')
a = dict_of_elements[1]
element_uniwersalny = classes.ElementUniwersalny(basevalues)
H, C, _ = element_uniwersalny.jacobian_for_element(a)
element_uniwersalny_edge = classes.ElementUniwersalnyEdges()
# print(element_uniwersalny_edge)
HC_matrix = H + C / basevalues['deltaTime']
grid_after = copy.deepcopy(grid)
H_global = np.array(np.zeros(shape=[number_of_nodes, number_of_nodes]))
# print(HC_matrix)

for idx, element in grid.list_of_elements.items():
    H_edge = np.zeros_like(H)
    if element.number_of_boundry_edges:
        for idx, value in enumerate(element.edges):
            if value:
                edge_detJ = element.get_detJ(idx)
                p1 = (3 + idx) % 4
                p2 = (3 + idx + 1) % 4
                tmp = np.zeros_like(H)
                for i in range(2):
                    tmp += np.outer(element_uniwersalny_edge.Edges[idx].N_arr[i],
                                    element_uniwersalny_edge.Edges[idx].N_arr[i])
                H_edge += basevalues['alpha'] * tmp * edge_detJ
    HC_matrix = H + H_edge + C / basevalues['deltaTime']
    for i in range(4):
        for j in range(4):
            H_global[element.ID[i].id - 1][element.ID[j].id - 1] += HC_matrix[i][j]
# with np.printoptions(precision=3, linewidth=1000):
#     print(H_global)

for time in range(basevalues['deltaTime'], basevalues['Time'], basevalues['deltaTime']):
    P_global = np.array(np.zeros(shape=[1, number_of_nodes]))
    P_elem = np.zeros((1, 4))
    PC_elem = np.zeros((1, 4))
    # print(C)

    for idx, element in grid.list_of_elements.items():
        _, _, PC_elem = element_uniwersalny.jacobian_for_element(element)
        if element.number_of_boundry_edges:
            for idx, value in enumerate(element.edges):
                for i in range(4):
                    if value:
                        edge_detJ = element.get_detJ(idx)
                        n2 = 0
                        for q in range(2):
                            for w in range(4):
                                n2 = element_uniwersalny_edge.Edges[idx].N_arr[q][w] /4
                                P_elem[0][w] += n2 * basevalues['alpha'] * basevalues['tinf'] * edge_detJ

        PC_elem += P_elem

    for idx, element in grid.list_of_elements.items():
        for point in range(4):
            P_global[0][element.ID[point].id - 1] += PC_elem[0][point]
    print(P_global)
