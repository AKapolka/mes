from math import *

import numpy as np


class Node():
    x = None
    y = None
    t = None
    id = None
    bc = False

    def __init__(self, x, y, t, id=0):
        self.x = float(x)
        self.y = float(y)
        self.t = float(t)
        self.id = id

    def set_id(self, id):
        self.id = id

    def set_boundary_condition(self, status):
        self.bc = status

    def __str__(self):
        if self.bc:
            string = "\nnode #{:<3} Edge\nx: {:<4} y: {:<4} t: {:<3}".format(self.id, self.x, self.y, self.t)
        else:
            string = "\nnode #{:<3}\nx: {:<4} y: {:<4} t: {:<3}".format(self.id, self.x, self.y, self.t)
        return string


class Element():
    id = 0
    ID = []
    K = None
    number_of_boundry_edges = 0

    def __init__(self, list_of_nodes, k, id=None):
        self.ID = list_of_nodes
        self.K = k
        if id is None:
            self.id = 0
        else:
            self.id = id
        self.edges = [self.ID[i % 4].bc and self.ID[(i + 1) % 4].bc for i in range(3, 7)]
        self.number_of_boundry_edges = sum(self.edges)

    def set_id(self, id):
        self.id = id

    def __str__(self):
        str1 = ''.join(str(x) for x in self.ID)
        return "\nElement #{:<3}{}\nBE:{}\nK: {:<3}".format(self.id, str1, self.number_of_boundry_edges, self.K)

    def coord_of_nodes(self):
        return [(node.x, node.y) for node in self.ID]

    def get_detJ(self, idx):
        return sqrt(pow(self.ID[(3 + idx) % 4].x - self.ID[(3 + idx + 1) % 4].x, 2)
                    + pow(self.ID[(3 + idx) % 4].y - self.ID[(3 + idx + 1) % 4].y, 2)) / 2


class Grid():
    list_of_nodes = None
    list_of_elements = None

    def __init__(self, nodes, elements):
        self.list_of_nodes = nodes
        self.list_of_elements = elements

    def __str__(self):
        str1 = ''
        for k in self.list_of_elements:
            str1 += str(self.list_of_elements[k]) + '\n'
        return str1


class ElementUniwersalny():
    arr1 = np.array([[-1, -1],
                     [1, -1],
                     [1, 1],
                     [-1, 1]])
    stala_ksi = 1 / sqrt(3)

    N_arr = np.empty(shape=[0, 4])
    dN_dksi_arr = np.empty(shape=[0, 4])
    dN_deta_arr = np.empty(shape=[0, 4])
    for i in range(len(arr1)):
        ksi = stala_ksi * arr1[i][0]  # ξ
        eta = stala_ksi * arr1[i][1]  # η
        N = []
        for j in range(4):
            N.append((1 / 4) * (1 + ksi * arr1[j][0]) * (1 + eta * arr1[j][1]))
        N_arr = np.append(N_arr, [N], axis=0)
        # print('\nPoint: #{:<}\nksi: {:<5}, eta: {:<5}'.format(i+1,ksi, eta))
        # print(*N, sep=' ')

        dN_dksi = []
        for j in range(4):
            dN_dksi.append((1 / 4) * arr1[j][0] * (1 + eta * arr1[j][1]))
        dN_dksi_arr = np.append(dN_dksi_arr, [dN_dksi], axis=0)
        # print(*dN_dksi, sep=' ')

        dN_deta = []
        for j in range(4):
            dN_deta.append((1 / 4) * arr1[j][1] * (1 + ksi * arr1[j][0]))
        dN_deta_arr = np.append(dN_deta_arr, [dN_deta], axis=0)
        # print(*dN_deta, sep=' ')

    dN_dksi_arrT = dN_dksi_arr.T
    dN_deta_arrT = dN_deta_arr.T

    basevalues = None

    def __init__(self, basevalues):
        self.basevalues = basevalues

    def jacobian_for_element(self, element):
        xy = element.coord_of_nodes()
        K = element.K
        # xy = [(10, 40), (80, 40), (50, 50), (20, 50)]
        J = []

        x_arr, y_arr = map(list, (zip(*xy)))
        for i in range(4):
            J.append(np.sum(np.multiply(self.dN_dksi_arr[i], np.array(x_arr))))
            J.append(np.sum(np.multiply(self.dN_dksi_arr[i], np.array(y_arr))))
            J.append(np.sum(np.multiply(self.dN_deta_arr[i], np.array(x_arr))))
            J.append(np.sum(np.multiply(self.dN_deta_arr[i], np.array(y_arr))))
        J = np.array(J).reshape((4, 4))
        # print(J)

        det_J = []
        for i in range(4):
            det_J.append(np.linalg.det(J[i].reshape((2, 2))))
        # print(det_J)
        J_detJ = np.empty((0, 4))
        for i in range(4):
            J_detJ = np.append(J_detJ, [np.linalg.inv(J[i].reshape((2, 2))).flatten()], axis=0)
        J_detJ = J_detJ.T
        # print(J_detJ)

        dN_dx = np.empty((0, 4))
        for i in range(4):
            dN_dx = np.append(dN_dx,
                              [J_detJ[0] * self.dN_dksi_arrT[i] +
                               J_detJ[1] * self.dN_deta_arrT[i]],
                              axis=0)
        dN_dx = dN_dx.T
        # print(dN_dx)

        dN_dy = np.empty((0, 4))
        for i in range(4):
            dN_dy = np.append(dN_dy,
                              [J_detJ[2] * self.dN_dksi_arrT[i] +
                               J_detJ[3] * self.dN_deta_arrT[i]],
                              axis=0)
        dN_dy = dN_dy.T
        # print(dN_dy)
        dN_dx2 = []
        dN_dy2 = []
        for i in range(4):
            dN_dx2.append(np.outer(dN_dx[i], dN_dx[i]) * det_J[i])
            dN_dy2.append(np.outer(dN_dy[i], dN_dy[i]) * det_J[i])
        # print(dN_dy2[0])
        preH = []
        for i in range(4):
            preH.append(K * (dN_dx2[i] + dN_dy2[i]))
        # print(preH[0])

        H = np.zeros((4, 4))
        for i in range(4):
            H += preH[i]
        # print(H)

        # print(self.N_arr)
        C = np.zeros((4, 4))
        const_value = self.basevalues["C"] * self.basevalues["R"]
        for i in range(4):
            C += np.outer(self.N_arr[i], self.N_arr[i]) * const_value * det_J[i]
            # print(C)

        T0 = [0, 0, 0, 0]
        for i in range(4):
            for j in range(4):
                T0[i] += self.N_arr[i][j] * element.ID[j].t

        C_pcal = np.zeros((1, 4))
        for i in range(4):
            for z in range(4):
                for x in range(4):

                    C_pcal[0][z] += (self.N_arr[i][z] * self.N_arr[i][x] * const_value * det_J[i]) / self.basevalues['deltaTime'] * \
                      T0[i]

        return H, C, C_pcal


class ElementUniwersalnyEdges():
    class ElementUniwersalnyEdge:
        N_arr = np.empty(shape=[0, 4])
        dN_dksi_arr = np.empty(shape=[0, 4])
        dN_deta_arr = np.empty(shape=[0, 4])
        local_nodes = None

        def __init__(self, nodes_tuple):
            self.local_nodes = nodes_tuple

        def __str__(self):
            outcome = ''
            for i in range(2):
                outcome += 'N:      ' + str(self.N_arr[i]) + '\ndN/dksi:' + str(
                    self.dN_dksi_arr[i]) + '\ndN/deta:' + str(self.dN_deta_arr[i]) + '\n'
            return outcome

    stala_ksi = 1 / sqrt(3)
    arr1 = np.array([[-1, -1],
                     [1, -1],
                     [1, 1],
                     [-1, 1]])

    ksi = [
        -1, -1,
        -stala_ksi, stala_ksi,
        1, 1,
        stala_ksi, -stala_ksi
    ]
    eta = [
        stala_ksi, -stala_ksi,
        -1, -1,
        -stala_ksi, stala_ksi,
        1, 1
    ]

    Edges = []

    for i in range(3, 7):
        Edges.append(ElementUniwersalnyEdge((i % 4, (i + 1) % 4)))

    N = []
    dN_dksi = []
    dN_deta = []
    for i in range(4):

        N.clear()
        for j in range(4):
            N.append((1 / 4) * (1 + ksi[i * 2] * arr1[j][0]) * (1 + eta[i * 2] * arr1[j][1]))
        Edges[i].N_arr = np.append(Edges[i].N_arr, [N], axis=0)
        N.clear()
        for j in range(4):
            N.append((1 / 4) * (1 + ksi[i * 2 + 1] * arr1[j][0]) * (1 + eta[i * 2 + 1] * arr1[j][1]))
        Edges[i].N_arr = np.append(Edges[i].N_arr, [N], axis=0)

        dN_dksi.clear()
        for j in range(4):
            dN_dksi.append((1 / 4) * arr1[j][0] * (1 + eta[i * 2] * arr1[j][1]))
        Edges[i].dN_dksi_arr = np.append(Edges[i].dN_dksi_arr, [dN_dksi], axis=0)
        dN_dksi.clear()
        for j in range(4):
            dN_dksi.append((1 / 4) * arr1[j][0] * (1 + eta[i * 2 + 1] * arr1[j][1]))
        Edges[i].dN_dksi_arr = np.append(Edges[i].dN_dksi_arr, [dN_dksi], axis=0)

        dN_deta.clear()
        for j in range(4):
            dN_deta.append((1 / 4) * arr1[j][1] * (1 + ksi[i * 2] * arr1[j][0]))
        Edges[i].dN_deta_arr = np.append(Edges[i].dN_deta_arr, [dN_deta], axis=0)
        dN_deta.clear()
        for j in range(4):
            dN_deta.append((1 / 4) * arr1[j][1] * (1 + ksi[i * 2 + 1] * arr1[j][0]))
        Edges[i].dN_deta_arr = np.append(Edges[i].dN_deta_arr, [dN_deta], axis=0)

    def __str__(self):
        output = ''
        for i in range(4):
            output += 'Krawedz {}\n'.format(i + 1)
            # output += 'Punkt całkowania #1: ksi = {:<4} eta = {:<4}\n'.format(self.ksi[i*2],self.eta[i*2])
            output += str(self.Edges[i])
        return output


pass
