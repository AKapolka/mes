import json


def loadfromfile():
    base_values = None
    with open('plik.json', 'r') as f:
        base_values = json.load(f)
        base_values['deltaH'] = base_values['H'] / (base_values['nH']-1)
        base_values['deltaL'] = base_values['L'] / (base_values['nL']-1)
    return base_values


if __name__ == '__main__':
    for k,v in loadfromfile().items():
        print(k,v)

