import numpy as np
from math import *

if __name__ == '__main__':
    arr1 = np.array([[-1, -1],
                     [1, -1],
                     [1, 1],
                     [-1, 1]])
    stala_ksi = 1 / sqrt(3)

    N_arr = np.empty(shape=[0, 4])
    dN_dksi_arr = np.empty(shape=[0, 4])
    dN_deta_arr = np.empty(shape=[0, 4])
    for i in range(len(arr1)):
        ksi = stala_ksi * arr1[i][0]  # ξ
        eta = stala_ksi * arr1[i][1]  # η
        N = []
        N.append((1 / 4) * (1 - ksi) * (1 - eta))  # N1
        N.append((1 / 4) * (1 + ksi) * (1 - eta))  # N2
        N.append((1 / 4) * (1 + ksi) * (1 + eta))  # N3
        N.append((1 / 4) * (1 - ksi) * (1 + eta))  # N4
        N_arr = np.append(N_arr, [N], axis=0)
        print('\nPoint: #{:<}\nksi: {:<5}, eta: {:<5}'.format(i+1,ksi, eta))
        print(*N, sep=' ')

        dN_dksi = []
        dN_dksi.append((1 / 4) * arr1[0][0] * (1 + eta * arr1[0][1]))
        dN_dksi.append((1 / 4) * arr1[1][0] * (1 + eta * arr1[1][1]))
        dN_dksi.append((1 / 4) * arr1[2][0] * (1 + eta * arr1[2][1]))
        dN_dksi.append((1 / 4) * arr1[3][0] * (1 + eta * arr1[3][1]))
        dN_dksi_arr = np.append(dN_dksi_arr, [dN_dksi], axis=0)
        # print(*dN_dksi, sep=' ')

        dN_deta = []
        dN_deta.append((1 / 4) * arr1[0][1] * (1 + ksi * arr1[0][0]))
        dN_deta.append((1 / 4) * arr1[1][1] * (1 + ksi * arr1[1][0]))
        dN_deta.append((1 / 4) * arr1[2][1] * (1 + ksi * arr1[2][0]))
        dN_deta.append((1 / 4) * arr1[3][1] * (1 + ksi * arr1[3][0]))
        dN_deta_arr = np.append(dN_deta_arr, [dN_deta], axis=0)
        # print(*dN_deta, sep=' ')

    # print(N_arr)
    # dN_dksi_arr = dN_dksi_arr.T
    # dN_deta_arr = dN_deta_arr.T
    # print(dN_dksi_arr)
    # print(dN_deta_arr)
    # for x in np.nditer(dN_deta_arr[0], order='C'):
    #     print(x, end=' ')
    # print()
    # xy = [(20, 40), (30, 40), (30, 50), (20, 50)]
    xy = [(10, 40), (80, 40), (50, 50), (20, 50)]
    test = np.empty_like(N_arr)
    # J = np.zeros_like(N_arr)
    J = []

    # print('test\n', J)

    x_arr, y_arr = map(list, (zip(*xy)))
    for i in range(4):
        J.append(np.sum(np.multiply(dN_dksi_arr[i], np.array(x_arr))))
        J.append(np.sum(np.multiply(dN_dksi_arr[i], np.array(y_arr))))
        J.append(np.sum(np.multiply(dN_deta_arr[i], np.array(x_arr))))
        J.append(np.sum(np.multiply(dN_deta_arr[i], np.array(y_arr))))
    J = np.array(J).reshape((4, 4)).round(10)
    print(J)

    det_J = []
    for i in range(4):
        det_J.append(np.linalg.det(J[i].reshape((2, 2))).round(4))
    print(det_J)

    J_detJ = np.empty((0, 4))
    for i in range(4):
        J_detJ = np.append(J_detJ, [J[i] / det_J[i]], axis=0).round(6)
        # print('j/detj\n', J[i], det_J[i])
    permutation = np.argsort([3, 1, 2, 0])
    J_detJ = J_detJ[:, permutation]
    print(J_detJ)
